package test;

import ATM.ATMMachine;
import org.junit.*;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

/**
 * Tests the ATM.
 */
public class ATMMachineTest {
    private static ATMMachine atm;
    private static Map<Integer, Integer> testMap;


    @BeforeClass
    public static void setup() {
        atm = new ATMMachine();
        testMap = new TreeMap<>(Collections.reverseOrder());
    }

    @Before
    public void restock() {
        atm.restockATM();

        testMap.put(100, 10);
        testMap.put(50, 10);
        testMap.put(20, 10);
        testMap.put(10, 10);
        testMap.put(5, 10);
        testMap.put(1, 10);
    }

    @Test
    public void testSuccessfulWithdrawal() {
        atm.withdraw(362);

        testMap.put(100, 7);
        testMap.put(50, 9);
        testMap.put(10, 9);
        testMap.put(1, 8);

        Assert.assertEquals(atm.getATMBills(), testMap);
    }

    @Test
    public void testIndividualBillValueWithdrawal() {
        atm.withdraw(100);
        atm.withdraw(50);
        atm.withdraw(20);
        atm.withdraw(10);
        atm.withdraw(5);
        atm.withdraw(1);

        testMap.put(100, 9);
        testMap.put(50, 9);
        testMap.put(20, 9);
        testMap.put(10, 9);
        testMap.put(5, 9);
        testMap.put(1, 9);

        Assert.assertEquals(atm.getATMBills(), testMap);
    }

    @Test
    public void testUnableToRetrieveExactAmount() {
        //These are valid
        Assert.assertTrue(atm.withdraw(4));
        Assert.assertTrue(atm.withdraw(4));

        testMap.put(1, 2);
        Assert.assertEquals(atm.getATMBills(), testMap);

        //This will fail to withdraw, as not enough 1's are left
        //The ATM should not be affected
        Assert.assertFalse(atm.withdraw(4));
        Assert.assertEquals(atm.getATMBills(), testMap);
    }

    @Test
    public void testWithdrawalInsufficientFunds() {
        Assert.assertFalse(atm.withdraw(2000));
    }

    @Test
    public void testRestock() {
        for (Integer numberOfBillsRemaining : atm.getATMBills().values()) {
            assertTrue(numberOfBillsRemaining == 10);
        }
    }

    @AfterClass
    public static void teardown() {
        atm = null;
    }
}
