package ATM;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A class that represents an ATM machine.
 */
public class ATMMachine {

    /**
     * Bills in the ATM.
     * Key: bill face value
     * Value: number of bills present in ATM
     */
    private Map<Integer, Integer> atmBills;

    /**
     * Stores the amount to retrieve during a withdrawal.
     */
    private int remainingAmountToRetrieve;

    /**
     * Initializes the ATM.
     */
    public ATMMachine() {
        atmBills = new TreeMap<>(Collections.reverseOrder());
        restockATM();
    }

    /**
     * Restocks the ATM to default amounts for each bill.
     */
    public void restockATM() {
        atmBills.put(100, 10);
        atmBills.put(50, 10);
        atmBills.put(20, 10);
        atmBills.put(10, 10);
        atmBills.put(5, 10);
        atmBills.put(1, 10);
    }

    /**
     * Prints the machine balance.
     *
     * @param billsToPrint: an array of bill values to print. If null, print all bill values.
     */
    public void printBalance(List<Integer> billsToPrint) {
        System.out.println("Machine balance:");
        if (billsToPrint == null) {
            atmBills.forEach((billFaceValue, numRemaining) -> System.out.println("$" + billFaceValue + " - " + numRemaining));
        } else {
            for (int billFaceValue : billsToPrint) {
                if (atmBills.containsKey(billFaceValue)) {
                    System.out.println("$" + billFaceValue + " - " + atmBills.get(billFaceValue));
                } else {
                    System.out.println("$" + billFaceValue + " - bill not an option");
                }
            }
        }

    }

    /**
     * Withdraws desired amount from the ATM.
     * <p>
     * If the withdrawal amount is larger than the amount in ATM
     * or the amount cannot be made using the bills in ATM,
     * an error message will be printed and the amount will not be withdrawn.
     *
     * @param retrievalAmount
     * @return true if withdrawal was successful
     */
    public boolean withdraw(int retrievalAmount) {
        Map<Integer, Integer> retrievalBills = new TreeMap<>();

        if (retrievalAmount <= totalMoneyRemaining()) {
            remainingAmountToRetrieve = retrievalAmount;

            //Determine if withdrawal of exact amount is possible
            atmBills.forEach((billFaceValue, numRemaining) -> {
                if (remainingAmountToRetrieve >= billFaceValue) {
                    int numBillsToRemove = Math.min(remainingAmountToRetrieve / billFaceValue, numRemaining);
                    retrievalBills.put(billFaceValue, numBillsToRemove);
                    remainingAmountToRetrieve -= billFaceValue * numBillsToRemove;
                }
            });

            //If withdrawal is possible, withdraw it
            if (remainingAmountToRetrieve == 0) {
                retrievalBills.forEach((billFaceValue, numBillsToRemove) ->
                        atmBills.put(billFaceValue, atmBills.get(billFaceValue) - numBillsToRemove)
                );
                System.out.println("Success: Dispensed $" + retrievalAmount);
            } else {
                System.out.println("Failure: Unable to retrieve amount");
                return false;
            }
            printBalance(null);
            return true;
        } else {
            System.out.println("Failure: Insufficient funds");
            return false;
        }
    }

    /**
     * Calculates the total amount of money present in the ATM.
     *
     * @return remaining money in ATM
     */
    public int totalMoneyRemaining() {
        int moneyRemaining = 0;

        for (int billFaceValue : atmBills.keySet()) {
            moneyRemaining += billFaceValue * atmBills.get(billFaceValue);
        }

        return moneyRemaining;
    }

    /**
     * Gets bills present in the ATM.
     * Used for testing purposes.
     *
     * @return Map of bills in the ATM.
     */
    public Map<Integer, Integer> getATMBills() {
        return atmBills;
    }
}
