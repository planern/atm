package ATM;

import java.util.ArrayList;

/**
 * A class to run the ATM.
 */
public class ATMRunner {
    /**
     * Main method.
     *
     * @param args: command line arguments.
     *              R: restocks the ATM.
     *              W<dollar amount>: withdraws the amount from the ATM.
     *              I<denominations>: displays the number of bills per denomination in the ATM.
     *              Q: quits the application.
     */
    public static void main(String[] args) {
        ATMMachine atm = new ATMMachine();

        for (int i = 0; i < args.length; i++) {
            switch (args[i].toUpperCase()) {
                case "R":
                    atm.restockATM();
                    atm.printBalance(null);
                    break;
                case "W":
                    if (i + 1 < args.length) {
                        try {
                            atm.withdraw(Integer.parseInt(args[++i].replace("$", "")));
                        } catch (NumberFormatException e) {
                            i--;
                            System.out.println("Failure: No value given to withdraw.");
                        }
                    } else {
                        System.out.println("Failure: No value given to withdraw.");
                    }
                    break;
                case "I":
                    boolean keepGoing = true;
                    ArrayList<Integer> billsToPrint = new ArrayList<>();

                    //Retrieve all bill values to print
                    while (keepGoing && i + 1 < args.length) {
                        try {
                            billsToPrint.add(Integer.parseInt(args[++i].replace("$", "")));
                        } catch (NumberFormatException e) {
                            i--;
                            keepGoing = false;
                        }
                    }

                    if (!billsToPrint.isEmpty()) {
                        atm.printBalance(billsToPrint);
                    } else {
                        atm.printBalance(null);
                    }

                    break;
                case "Q":
                    break;
                //Invalid command entered. Non-integer values are always treated as invalid commands.
                default:
                    System.out.println("Failure: Invalid Command");
                    break;
            }
        }
    }
}
